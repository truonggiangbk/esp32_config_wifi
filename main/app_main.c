/*  WiFi softAP Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include "app_http_server.h"
#include "app_mqtt.h"

#include "json_parser.h"
#include "json_generator.h"
#include "output_iot.h"
#include "app_flash.h"
#include "app_ota.h"

typedef enum{
    CONFIG_WIFI_ACCESSPOINT = 0,
    CONFIG_WIFI_SMARTCONFIG = 1,
}   config_wifi_mode_t;

typedef struct {
    char buf[256];
    size_t offset;
} json_gen_test_result_t;

/* The examples use WiFi configuration that you can set via project configuration menu.

   If you'd rather not, just change the below entries to strings with
   the config you want - ie #define EXAMPLE_WIFI_SSID "mywifissid"
*/
#define EXAMPLE_ESP_WIFI_SSID      "Deviot.vn"
#define EXAMPLE_ESP_WIFI_PASS      "12345678"
#define EXAMPLE_ESP_WIFI_CHANNEL   1
#define EXAMPLE_MAX_STA_CONN       4

static const char *TAG = "wifi softAP";
static json_gen_test_result_t result;

static char ssid[30];
static char pass[30];
static config_wifi_mode_t config_mode = CONFIG_WIFI_SMARTCONFIG;

static EventGroupHandle_t s_wifi_event_group;
#define WIFI_FAIL_BIT	        ( 1 << 0 )
#define WIFI_CONNECTED_BIT	    ( 1 << 1 )
#define WIFI_CONFIG_BIT	    ( 1 << 1 )

static void wifi_event_handler(void* arg, esp_event_base_t event_base,
                                    int32_t event_id, void* event_data)
{
    int s_retry_num = 0;

    // event of accesspoint
    if (event_id == WIFI_EVENT_AP_STACONNECTED) {
        wifi_event_ap_staconnected_t* event = (wifi_event_ap_staconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" join, AID=%d",
                 MAC2STR(event->mac), event->aid);
    } else if (event_id == WIFI_EVENT_AP_STADISCONNECTED) {
        wifi_event_ap_stadisconnected_t* event = (wifi_event_ap_stadisconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" leave, AID=%d",
                 MAC2STR(event->mac), event->aid);
    }

    // event of station
    if (event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if (s_retry_num < 5) {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        } else {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(TAG,"connect to the AP fail");
    } else if (event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }    
}

void wifi_init_softap(void)
{
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    wifi_config_t wifi_config = {
        .ap = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .ssid_len = strlen(EXAMPLE_ESP_WIFI_SSID),
            .channel = EXAMPLE_ESP_WIFI_CHANNEL,
            .password = EXAMPLE_ESP_WIFI_PASS,
            .max_connection = EXAMPLE_MAX_STA_CONN,
            .authmode = WIFI_AUTH_WPA_WPA2_PSK
        },
    };
    if (strlen(EXAMPLE_ESP_WIFI_PASS) == 0) {
        wifi_config.ap.authmode = WIFI_AUTH_OPEN;
    }

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_LOGI(TAG, "wifi_init_softap finished. SSID:%s password:%s channel:%d",
             EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS, EXAMPLE_ESP_WIFI_CHANNEL);
}

void wifi_init_sta(void)
{
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = "",
            .password = "",
            /* Setting a password implies station will connect to all security modes including WEP/WPA.
             * However these modes are deprecated and not advisable to be used. Incase your Access point
             * doesn't support WPA2, these mode can be enabled by commenting below line */
	     .threshold.authmode = WIFI_AUTH_WPA2_PSK,

            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };

    strcpy((char*)wifi_config.sta.ssid, ssid);
    strcpy((char*)wifi_config.sta.password, pass);

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );
}

void http_post_callback(char *data, int len)
{
    char*pt = strtok(data, "-");
    strcpy(ssid, pt);
    pt = strtok(NULL, "-");
    len = len - strlen(ssid) - 1;
    strcpy(pass, pt);
    pass[len] = '\0';
    ESP_LOGW(TAG, "ssid: %s", ssid);
    ESP_LOGW(TAG, "pass: %s", pass);
    xEventGroupSetBits(s_wifi_event_group, WIFI_CONFIG_BIT);
}

int gpio;
bool onoff;

static void flush_str(char *buf, void *priv)
{
    json_gen_test_result_t *result = (json_gen_test_result_t *)priv;
    if (result) {
        if (strlen(buf) > sizeof(result->buf) - result->offset) {
            printf("Result Buffer too small\r\n");
            return;
        }
        memcpy(result->buf + result->offset, buf, strlen(buf));
        result->offset += strlen(buf);
    }
}

int json_parse(char *json, int len)
{
    jparse_ctx_t jctx;
    int ret = json_parse_start(&jctx, json, len);
	if (ret != OS_SUCCESS) {
		printf("Parser 1 failed\n");
		return -1;
	}   
    if (json_obj_get_int(&jctx, "gpio", &gpio) != OS_SUCCESS) 
    {
		printf("Parser 2 failed\n");
		return -1;
    }
    if (json_obj_get_bool(&jctx, "onoff", &onoff) != OS_SUCCESS)
    {
		printf("Parser 3 failed\n");
		return -1;        
    }
    json_parse_end(&jctx);
    return 0;
}

int json_gen(json_gen_test_result_t *result, char *key1, int value1, char *key2, bool value2)
{
	char buf[20];
    memset(result, 0, sizeof(json_gen_test_result_t));
	json_gen_str_t jstr;    
    json_gen_str_start(&jstr, buf, sizeof(buf), flush_str, result);
	json_gen_start_object(&jstr);    
	
	json_gen_obj_set_int(&jstr, key1, value1);  
    json_gen_obj_set_bool(&jstr, key2, value2);  

	json_gen_end_object(&jstr);
	json_gen_str_end(&jstr);

    return 0;
}

void mqtt_data_callback(char *dt, int len)
{
    dt[len] = '\0';
    printf("DATA=%s\r\n", dt);    

    if(strstr(dt, "ota")){
        ota_task_init_start();
    }

    json_parse(dt, len);
    if(gpio == 2){
        if(onoff == true)
        {
            output_io_set_level(2, 1);
        }
        else{
            output_io_set_level(2, 0);
        }
    }
}

void app_main(void)
{
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    config_mode = app_flash_get_config_mode();
    config_mode = 1 - config_mode;
    app_flash_set_config_mode(config_mode);

    app_flash_set_home_id("HOME12345678");
    char * pt = app_flash_get_home_id();
    printf("get homeid = %s\n", pt);

    output_io_create(2);
    output_io_set_level(2, 0);

    json_gen(&result, "gpio", 2, "onoff", true);
    printf("JSON: %s", result.buf);

    mqtt_app_init();
    mqtt_set_callback(mqtt_data_callback);

    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        NULL));

    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        NULL));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        NULL));

    esp_netif_create_default_wifi_ap();
    esp_netif_create_default_wifi_sta();

    s_wifi_event_group = xEventGroupCreate();

    ESP_LOGI(TAG, "ESP_WIFI_MODE_AP");
    wifi_init_softap();
    http_post_set_callback(http_post_callback);
    start_webserver();
    xEventGroupWaitBits(s_wifi_event_group, WIFI_CONFIG_BIT, pdTRUE,pdFALSE, portMAX_DELAY);
    stop_webserver();
    wifi_init_sta();
    xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT, pdTRUE,pdFALSE, portMAX_DELAY);
    mqtt_app_start();
}
